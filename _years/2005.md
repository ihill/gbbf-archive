---
organiser: Marc Holmes
venue: Olympia
---

Chief Steward
: Robin Lacy

Head of Technical
: Kim Kellaway

CBOB winner
: Crouch Vale, Brewers Gold

Logo
: 2005 with first 0 as a pint [Picture]({% file logo.jpg %})

Halls
: Grand, National, West

Links
: [Programme]({% file programme.jpg %})
[Floor plan]({% file floorplan.jpg %})

### Opening hours
Tuesday	
: Trade 12pm - 5pm, Public 5pm - 10:30pm

Wednesday
: 12 noon - 10:30pm

Thursday
: 12 noon - 10:30pm

Friday
: 12 noon - 10:30pm

Saturday
: 11am - 7pm

### Sponsors and supporters

Charity
: Royal British Legion

Staff T-shirt
: Ringwood, Fortyniner
[Front]({% file shirt_front.jpg %})
[Back]({% file shirt_back.jpg %})

### Bars

11 + Brewery Bars + RAIB + Cider + BSF

**Brewery Bars**
Kiz
: Adnams, Shepherd Neame, Batemans, Cains

Steve
: Gales, Wolverhampton & Dudley, Fullers, Youngs 

Linda
: Theakstons, Badger, Hook Norton, Greene King 

JC
: Charles Wells, Thwaites, Wychwood, Sharp's 

Plus... 
: Oakham, Woodfordes (in National attached to Bars S and T)

### Entertainment

Tuesday Eve
: Chaminade

Wednesday lunch
: Band of Two

Wednesday evening
: Chas'n'Dave

Thursday lunch
: Gordon Giltrap

Thursday evening
: Los Pacaminos

Friday lunch
: Grapevine Blues

Friday evening
: The Rollin' Stoned

Saturday afternoon
: National Youth Jazz Orchestra

### Behind the scenes

Wenches' Night Theme	
: Emergency Services

Night Stewards Prank
: Buttercup goes upstairs

Christian Muteau Award	
: Roy Jenner
