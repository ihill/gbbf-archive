---
organiser: Marc Holmes
venue: Olympia
---

Chief Steward
: Duncan Ward

Head of Technical
: Kim Kellaway

CBOB winner
: Harviestoun, Bitter & Twisted

Logo
: 2003 with first 0 as a pint [Picture]({% file logo.jpg %})

Halls
: Grand, National, West

Links
: [Programme]({% file programme.jpg %})
[Floor plan]({% file floorplan.jpg %})

### Opening hours
Tuesday	
: Trade 12pm - 5pm, Public 5pm - 10:30pm

Wednesday
: 12 noon - 10:30pm

Thursday
: 12 noon - 10:30pm

Friday
: 12 noon - 10:30pm

Saturday
: 11am - 7pm

### Sponsors and supporters

Charity
: British Heart Foundation

Staff T-shirt
: Wadworth, 6x Therapist
[Front]({% file shirt_front.jpg %})
[Back]({% file shirt_back.jpg %})


### Bars

10 + Brewery Bars + RAIB + Cider + BSF

**Brewery Bars**
Kiz
: Adnams, Shepherd Neame, Batemans, Everards, Gales 

Steve
: Marstons, Fullers, Youngs, Cains, Oakham

Nick
: St Austell, Courage, Hook Norton, Ridley 

Linda
: Thwaites, Tetley, Badger, Charles Wells

### Entertainment

Tuesday Eve
: Chaminade

Wednesday lunch
: Zambula

Wednesday evening
: Lindesfarne

Thursday lunch
: John Pawlik

Thursday evening
: John Otway

Friday lunch
: Jazzy Trousers

Friday evening
: The Hamsters

Saturday afternoon
: Fulham Brass Band

### Behind the scenes

Wenches' Night Theme	
: Horror

Night Stewards Prank
: My Little Pony goes to the beach

Christian Muteau Award	
: Lynda Smith
