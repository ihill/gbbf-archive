# Get the path for a given static file, associated with this page
# {% file test.jpg %} in _years/2011.md will return the path to
# static/year/2011/test.jpg
#
# Modified from https://github.com/samrayner/jekyll-asset-path-plugin

module Jekyll

  class AssetPathTag < Liquid::Tag
    @markup = nil

    def initialize(tag_name, markup, tokens)
      @markup = markup.strip
      super
    end

    def render(context)
      if @markup.empty?
        return "Error processing input, expected syntax: {% file filename %}"
      end

      #render the markup
      parameters = Liquid::Template.parse(@markup).render context
      parameters.strip!

      if ['"', "'"].include? parameters[0] 
        # Quoted filename
        last_quote_index = parameters.rindex(parameters[0])
        filename = parameters[1 ... last_quote_index]
      else
        # Unquoted filename
        filename = parameters
      end

      page = context.environments.first["page"]
      path = page["url"]

      #strip filename
      path = File.dirname(path) if path =~ /\.\w+$/

      #fix double slashes
      "#{context.registers[:site].config['baseurl']}/static/#{path}/#{filename}".gsub(/\/{2,}/, '/')
    end
  end
end

Liquid::Template.register_tag('file', Jekyll::AssetPathTag)
